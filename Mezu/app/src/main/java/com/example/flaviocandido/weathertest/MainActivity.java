package com.example.flaviocandido.weathertest;

import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.ViewPropertyTransition;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    //region Globalvariables
    ImageView Iv_imagem_tempo;
    TextView Tv_temperatura, Tv_descricao, Tv_local;
    String Url_base_inicio = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22";
    String Url_base_fim = "%22)%20and%20u%3D%22c%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    PlaceAutocompleteFragment placeAutocompleteFragment;
    LinearLayout ll_main_layout;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //endregion

        //region DeclareElements
        Iv_imagem_tempo = findViewById(R.id.iv_imagem_tempo);
        Tv_temperatura = findViewById(R.id.tv_temperatura);
        Tv_descricao = findViewById(R.id.tv_descricao);
        Tv_local = findViewById(R.id.tv_local);
        ll_main_layout = findViewById(R.id.ll_main_layout);
        placeAutocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        //endregion

        //region Autocomplete
        //I decided to use google places API to get the correct address when user change the location

        //region Filter
        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();
        placeAutocompleteFragment.setFilter(autocompleteFilter);
        placeAutocompleteFragment.setHint(getString(R.string.hint_autocomplete));
        //endregion

        //region PlaceSelected
        placeAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                //region GetTemperatureByName
                //get the address and send it to Weather Yahoo API
                getTemperature(place.getAddress().toString());
                //endregion
            }

            @Override
            public void onError(Status status) {
                Toast.makeText(getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        //endregion

        //region ClearPlace
        placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_clear_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {
                            hideContent();
                        } catch (Exception e) {
                            //should send the error to google analytics
                        }

                    }
                });
        //endregion

        //endregion

    }

    @Override
    public void onStop() {
        super.onStop();

        //region Hide the contents of the app when switching to other app or home screen.
        hideContent();
        //endregion
    }

    public void hideContent() {

        //region ClearData
        Iv_imagem_tempo.setImageBitmap(null);
        Tv_temperatura.setText("");
        Tv_descricao.setText("");
        Tv_local.setText("");
        placeAutocompleteFragment.setText("");
        //endregion

    }

    public void getTemperature(String local) {

        //region UrlEncode
        try {
            local = URLEncoder.encode(local, "UTF-8");
        } catch (Exception error) {
            //should send the error to google analytics
        }
        //endregion

        //region GetTemperature
        Ion.with(getApplicationContext())

                .load(Url_base_inicio + local + Url_base_fim)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        if (e == null) {

                            //region GetResponse
                            JsonObject query = result.getAsJsonObject("query");
                            JsonObject resultado = query.getAsJsonObject("results");
                            JsonObject canal = resultado.getAsJsonObject("channel");
                            //endregion

                            //region Location
                            JsonObject location = canal.getAsJsonObject("location");
                            String cidade = location.get("city").getAsString();
                            String pais = location.get("country").getAsString();
                            String local = cidade + ", " + pais;

                            //set text local
                            Tv_local.setText(local);
                            //endregion

                            //region Temperature
                            JsonObject item = canal.getAsJsonObject("item");
                            JsonObject conditions = item.getAsJsonObject("condition");
                            String temperatura = conditions.get("temp").getAsString() + getString(R.string.graus);

                            //set text temperatura
                            Tv_temperatura.setText(temperatura);
                            //endregion

                            //region Descricao
                            String descricao = conditions.get("text").getAsString();

                            //set text descricao
                            Tv_descricao.setText(descricao);
                            //endregion

                            //region Img
                            String descritionWithImgUrl = item.get("description").getAsString();
                            //using regex we can find where is the weather image url
                            String[] output = descritionWithImgUrl.split("img src=\"");
                            String[] output2 = output[1].split("\"");
                            String url_imagem = output2[0];

                            //set Image using glide
                            setImg(Iv_imagem_tempo, url_imagem);
                            //endregion

                        } else {

                            //region OcorreuErro
                            Toast.makeText(getApplicationContext(), R.string.msg_de_erro_obter_temperatura, Toast.LENGTH_SHORT).show();
                            //endregion
                        }
                    }
                });
        //endregion
    }

    private void setImg(ImageView img_weather, String url) {

        try {

            GlideUrl urlGlideWithHeader = new GlideUrl(url, new LazyHeaders.Builder()
                    .addHeader("Content-Type", "application/json")
                    .build());
            Glide
                    .with(getApplicationContext())
                    .load(urlGlideWithHeader)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                            return false;
                        }
                    })
                    .transition(GenericTransitionOptions.with(animacao))
                    .into(img_weather);
        } catch (Exception e) {
            //should send the error to google analytics
        }

    }

    final ViewPropertyTransition.Animator animacao = new ViewPropertyTransition.Animator() {
        @Override
        public void animate(View view) {

            //region imageAnimationFadeIn
            view.setAlpha(0f);
            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            fadeAnim.setDuration(1000);
            fadeAnim.start();
            //endregion
        }
    };

}
